﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Net5Test2.BLL;
using Net5Test2.BLL.Data.Models;
using Net5Test2.Models;

namespace Net5Test2.Controllers
{
    public class HomeController : Controller
    {
        private readonly MyDataService _dataService;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, MyDataService dataService)
        {
            _logger = logger;
            _dataService = dataService;
        }

        public IActionResult Index()
        {
            var users = _dataService.GetUsers();
            return View(users);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Save(User user)
        {
            user.CreationDateTime = DateTime.Now;
            user.LastUpdateDateTime = DateTime.Now;
            
            _dataService.Save(user);
            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}